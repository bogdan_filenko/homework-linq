using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Immutable;
using homework_linq.BLL.Interfaces;
using homework_linq.BLL.Models;
using homework_linq.BLL.Dto;

namespace homework_linq.BLL.Services
{
    public class DataComposeService : IDataComposeService
    {
        private readonly string _routePrefix;
        private readonly IHttpClientService _httpClientService;
        private readonly IMapper _mapper;

        private ImmutableArray<ProjectDto> projectsDtoList;
        private ImmutableArray<TaskDto> tasksDtoList;
        private ImmutableArray<TeamDto> teamsDtoList;
        private ImmutableArray<UserDto> usersDtoList;

        public DataComposeService(IHttpClientService httpClientService, IMapper mapper)
        {
            _httpClientService = httpClientService;
            _mapper = mapper;
            _routePrefix = "https://bsa21.azurewebsites.net/api";
        }
        public async Task<ImmutableArray<Project>> GetComposedDataAsync()
        {
            projectsDtoList = await _httpClientService.GetAllEntitiesAsync<ProjectDto>(_routePrefix + "/Projects");
            tasksDtoList = await _httpClientService.GetAllEntitiesAsync<TaskDto>(_routePrefix + "/Tasks");
            teamsDtoList = await _httpClientService.GetAllEntitiesAsync<TeamDto>(_routePrefix + "/Teams");
            usersDtoList = await _httpClientService.GetAllEntitiesAsync<UserDto>(_routePrefix + "/Users");

            var composedUsers = GetComposedUsersList();
            var composedTasks = GetComposedTasksList(composedUsers);
            var composedTeams = GetComposedTeamsList(composedUsers);
            var composedProjects = GetComposedProjectsList(composedUsers, composedTeams, composedTasks);

            return composedProjects.ToImmutableArray();
        }

        private IEnumerable<Project> GetComposedProjectsList(IEnumerable<User> composedUsers, IEnumerable<Team> composedTeams, IEnumerable<Models.Task> composedTasks)
        {
            var composedProjects = from projectDto in projectsDtoList
                join team in composedTeams on projectDto.TeamId equals team.Id
                join user in composedUsers on projectDto.AuthorId equals user.Id
                select _mapper.Map<Project>(projectDto, opts =>
                {
                    opts.AfterMap((o, project) =>
                    {
                        project.Team = team;
                        project.Author = user;
                    });
                });

            return composedProjects = composedProjects.GroupJoin(
                composedTasks,
                p => p.Id,
                t => t.Project.Id,
                (p, t) =>
                {
                    p.Tasks = t.ToArray();
                    return p;
                }
            );
        }

        private IEnumerable<Team> GetComposedTeamsList(IEnumerable<User> composedUsers)
        {
            return teamsDtoList.GroupJoin(
                composedUsers,
                t => t.Id,
                u => u.Team.Id,
                (t, u) => _mapper.Map<Team>(t, opts =>
                {
                    opts.AfterMap((o, team) =>
                    {
                        team.Users = u.ToArray();
                    });
                })
            );
        }

        private IEnumerable<Models.Task> GetComposedTasksList(IEnumerable<User> composedUsers)
        {
            return from taskDto in tasksDtoList
                join projectDto in projectsDtoList on taskDto.ProjectId equals projectDto.Id
                join performer in composedUsers on taskDto.PerformerId equals performer.Id
                select _mapper.Map<Models.Task>(taskDto, opts =>
                {
                    opts.AfterMap((o, task) =>
                    {
                        task.Performer = _mapper.Map<User>(performer);
                        task.Project = _mapper.Map<Project>(projectDto);
                    });
                });
        }

        private IEnumerable<User> GetComposedUsersList()
        {
            var composedUsers = from userDto in usersDtoList
                join teamDto in teamsDtoList on userDto.TeamId equals teamDto.Id
                select _mapper.Map<User>(userDto, opts =>
                {
                    opts.AfterMap((o, user) =>
                    {
                        user.Team = _mapper.Map<Team>(teamDto);
                    });
                });
            
            return composedUsers.GroupJoin(
                tasksDtoList,
                u => u.Id,
                t => t.PerformerId,
                (u, t) =>
                {
                    u.Tasks = _mapper.Map<Models.Task[]>(t);
                    return u;
                }
            )
            .GroupJoin(
                projectsDtoList,
                u => u.Id,
                p => p.AuthorId,
                (u, p) =>
                {
                    u.Projects = _mapper.Map<Project[]>(p);
                    return u;
                }
            );
        }
    }
}