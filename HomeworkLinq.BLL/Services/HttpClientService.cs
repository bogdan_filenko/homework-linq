using System.Threading.Tasks;
using System.Net.Http;
using System.Collections.Immutable;
using homework_linq.BLL.Interfaces;
using Newtonsoft.Json;

namespace homework_linq.BLL.Services
{
    public class HttpClientService : IHttpClientService
    {
        private readonly HttpClient _client;
        public HttpClientService()
        {
            _client = new HttpClient();
        }
        public async Task<ImmutableArray<T>> GetAllEntitiesAsync<T>(string url)
        {
            string entitiesJson = await _client.GetStringAsync(url);
            var entitiesDtoList = JsonConvert.DeserializeObject<ImmutableArray<T>>(entitiesJson);
            
            return entitiesDtoList;
        }
        public async Task<T> GetEntityAsync<T>(string url)
        {
            string entityJson = await _client.GetStringAsync(url);
            var entityDto = JsonConvert.DeserializeObject<T>(entityJson);
            
            return entityDto;
        }

        public void Dispose()
        {
            _client.Dispose();
        }
    }
}