using AutoMapper;
using homework_linq.BLL.Dto;
using homework_linq.BLL.Models;

namespace homework_linq.BLL.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<TeamDto, Team>();

            CreateMap<Team, ShortTeam>();
        }
    }
}