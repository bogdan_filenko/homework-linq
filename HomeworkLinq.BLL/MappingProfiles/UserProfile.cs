using AutoMapper;
using homework_linq.BLL.Dto;
using homework_linq.BLL.Models;

namespace homework_linq.BLL.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserDto, User>();
        }
    }
}