using AutoMapper;
using homework_linq.BLL.Dto;
using homework_linq.BLL.Models;

namespace homework_linq.BLL.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<TaskDto, Task>()
                .ForMember(dest => dest.State, (src) => src.MapFrom(t => (TaskState)t.State));

            CreateMap<Task, ShortTask>();
        }
    }
}