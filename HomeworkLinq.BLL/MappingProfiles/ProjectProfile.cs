using AutoMapper;
using homework_linq.BLL.Dto;
using homework_linq.BLL.Models;

namespace homework_linq.BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<ProjectDto, Project>();
        }
    }
}