using System;

#nullable enable

namespace homework_linq.BLL.Dto
{
    public sealed class ProjectDto
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public int AuthorId { get; set; }
        public int TeamId { get; set; }
    }
}