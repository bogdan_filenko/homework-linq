using System;

#nullable enable

namespace homework_linq.BLL.Dto
{
    public sealed class TeamDto
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}