using System;

#nullable enable

namespace homework_linq.BLL.Dto
{
    public sealed class UserDto
    {
        public int Id { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDate { get; set; }

        public int? TeamId { get; set; }
    }
}