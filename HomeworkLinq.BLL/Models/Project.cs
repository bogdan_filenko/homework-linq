using System;

#nullable enable

namespace homework_linq.BLL.Models
{
    public sealed class Project
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public Task[]? Tasks { get; set; }
        public User? Author { get; set; }
        public Team? Team { get; set; }

        public override string ToString()
        {
            return "Project:\n" +
                    $"\tId: {this.Id}\n" +
                    $"\tName: {this.Name}\n" +
                    $"\tDescription: {this.Description}\n" +
                    $"\tDeadline: {this.Deadline.ToString()}\n" +
                    $"\tCreated at: {this.CreatedAt.ToString()}";
        }
    }
}