#nullable enable

namespace homework_linq.BLL.Models
{
    public sealed class ShortTask
    {
        public int Id { get; set; }
        public string? Name { get; set; }

        public override string ToString()
        {
            return  $"Id: {this.Id}\n" +
                    $"Name: {this.Name}";
        }
    }
}