using System;
using System.Text;

#nullable enable

namespace homework_linq.BLL.Models
{
    public sealed class Team
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public Project[]? Projects { get; set; }
        public User[]? Users { get; set; }

        public override string ToString()
        {
            StringBuilder viewBuilder = new StringBuilder();
            
            viewBuilder.Append( $"Id: {this.Id}\n" +
                                $"Name: {this.Name}\n" +
                                $"Created at: {this.CreatedAt.ToString()}");
            
            return viewBuilder.ToString();
        }
    }
}