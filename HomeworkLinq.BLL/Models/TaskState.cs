public enum TaskState : byte
{
    Added,
    InProcess,
    Finished,
    Refused
}