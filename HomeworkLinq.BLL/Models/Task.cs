using System;

#nullable enable

namespace homework_linq.BLL.Models
{
    public sealed class Task
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public TaskState State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }

        public Project? Project { get; set; }
        public User? Performer { get; set; }

        public override string ToString()
        {
            return  $"Id: {this.Id}\n" +
                    $"Name: {this.Name}\n" +
                    $"Description: {this.Description}\n" +
                    $"Created at: {this.CreatedAt}\n" +
                    $"Finished at: {(this.FinishedAt == default ? "Still in process" : this.FinishedAt.ToString())}\n" +
                    $"Performer: {this.Performer.FirstName + " " + this.Performer.LastName}";
        }
    }
}