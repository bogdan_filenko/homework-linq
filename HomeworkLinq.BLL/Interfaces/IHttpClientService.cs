using System;
using System.Threading.Tasks;
using System.Collections.Immutable;

namespace homework_linq.BLL.Interfaces
{
    public interface IHttpClientService : IDisposable
    {
        Task<ImmutableArray<T>> GetAllEntitiesAsync<T>(string url);
        Task<T> GetEntityAsync<T>(string url);
    }
}