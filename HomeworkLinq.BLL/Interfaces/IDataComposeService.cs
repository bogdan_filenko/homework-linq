using System.Threading.Tasks;
using System.Collections.Immutable;
using homework_linq.BLL.Models;

namespace homework_linq.BLL.Interfaces
{
    public interface IDataComposeService
    {
        Task<ImmutableArray<Project>> GetComposedDataAsync();
    }
}