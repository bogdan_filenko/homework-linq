using homework_linq.BLL.Models;
using System.Collections.Immutable;
using System;

namespace homework_linq.Client
{
    public sealed class ResponseDataViewer
    {
        public void ViewProjectsTaskNumber(ImmutableDictionary<Project, int> responseData)
        {
            Console.Clear();

            if (responseData != default)
            {
                foreach (var projectNumberPair in responseData)
                {
                    Console.WriteLine(
                        projectNumberPair.Key.ToString() + '\n' +
                        $"Tasks` number: {projectNumberPair.Value}\n"
                        );
                }
            }
        }
        public void ViewCollectionData<T>(ImmutableArray<T> responseData)
        {
            Console.Clear();

            if (responseData != default)
            {
                foreach (var entity in responseData)
                {
                    Console.WriteLine(entity.ToString() + '\n');
                }
            }
        }
        public void ViewSingleEntity<T>(T responseData)
        {
            Console.Clear();

            if (responseData != null)
            {
                Console.WriteLine(responseData.ToString() + '\n');
            }
        }
    }
}