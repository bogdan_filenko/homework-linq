using System.Threading.Tasks;
using System.Linq;
using System;
using System.Collections.Immutable;
using System.Collections.Generic;
using homework_linq.BLL.Models;
using AutoMapper;
using homework_linq.BLL.Interfaces;
using homework_linq.BLL.Services;
using homework_linq.Client.DistinctComparers;

namespace homework_linq.Client
{
    public sealed class ComposedDataController
    {
        private readonly IDataComposeService _dataComposeService;
        private readonly IMapper _mapper;

        private const int CURRENT_YEAR = 2021;

        public ComposedDataController(IMapper mapper)
        {
            _mapper = mapper;
            _dataComposeService = new DataComposeService(
                new HttpClientService(),
                _mapper
            );
        }
        public async Task<ImmutableDictionary<Project, int>> GetProjectsTaskNumber(int userId)
        {
            var composedData = await _dataComposeService.GetComposedDataAsync();

            return composedData
                .Where(p => p.Author.Id == userId)
                .Select(p => new KeyValuePair<Project, int>(
                    new Project
                    {
                        Id = p.Id,
                        Name = p.Name,
                        Description = p.Description,
                        Deadline = p.Deadline,
                        CreatedAt = p.CreatedAt
                    },
                    p.Tasks.Length
                ))
                .ToImmutableDictionary();
        }

        public async Task<ImmutableArray<homework_linq.BLL.Models.Task>> GetAllUserTasks(int userId)
        {
            var composedData = await _dataComposeService.GetComposedDataAsync();

            return composedData
                .Aggregate(new List<homework_linq.BLL.Models.Task>(), (x, y) =>
                    x.Concat(y.Tasks).ToList())
                .Where(t => t.Performer.Id == userId && t.Name.Length < 45)
                .ToImmutableArray();
        }

        public async Task<ImmutableArray<ShortTask>> GetFinishedTasks(int userId)
        {
            var composedData = await _dataComposeService.GetComposedDataAsync();

            return composedData
                .Aggregate(new List<homework_linq.BLL.Models.Task>(), (x, y) =>
                    x.Concat(y.Tasks).ToList())
                .Where(t => t.FinishedAt != default && t.FinishedAt.Value.Year == CURRENT_YEAR && t.Performer.Id == userId)
                .Select(t => _mapper.Map<ShortTask>(t))
                .ToImmutableArray();
        }
        
        public async Task<ImmutableArray<ShortTeam>> GetTeamsWhereUsersOlderThan10()
        {
            var composeData = await _dataComposeService.GetComposedDataAsync();

            return composeData
                .Aggregate(new List<Team>(), (x, y) =>
                    x.Append(y.Team).ToList())
                .Distinct()
                .Where(t => t.Users.All(u => DateTime.Now.Year - u.BirthDate.Year > 10))
                .Select(t => _mapper.Map<ShortTeam>(t, opts =>
                {
                    opts.AfterMap((o, team) =>
                    {
                        team.Users = team.Users.OrderByDescending(u => u.RegisteredAt).ToArray();
                    });
                }))
                .ToImmutableArray();
        }
        
        public async Task<ImmutableArray<User>> GetUsersWithSortedTasks()
        {
            var composedData = await _dataComposeService.GetComposedDataAsync();

            return composedData
                .Aggregate(new List<User>(), (x, y) =>
                    x.Concat(y.Team.Users)
                    .ToList()
                )
                .Select(u => new User()
                {
                    Id = u.Id,
                    FirstName = u.FirstName,
                    LastName = u.LastName,
                    BirthDate = u.BirthDate,
                    Email = u.Email,
                    RegisteredAt = u.RegisteredAt,
                    Team = u.Team,
                    Tasks = u.Tasks.OrderByDescending(t => t.Name.Length).ToArray()
                })
                .Distinct(new UserDistinctComparer())
                .OrderBy(u => u.FirstName)
                .ToImmutableArray();
        }

        public async Task<UserInfo> GetUserInfo(int userId)
        {
            var composedData = await _dataComposeService.GetComposedDataAsync();

            return composedData
                .Aggregate(new List<User>(), (x, y) =>
                    x.Concat(y.Team.Users)
                    .ToList()
                )
                .Distinct(new UserDistinctComparer())
                .Where(u => u.Id == userId)
                .Select(u => new UserInfo()
                {
                    User = u,
                    LastProject = u.Projects
                        .OrderByDescending(p => p.CreatedAt)
                        .FirstOrDefault(),
                    LastProjectTotalTasks = u.Projects
                        .OrderByDescending(p => p.CreatedAt)
                        .FirstOrDefault()?.Tasks?.Length ?? 0,
                    TotalUnfinishedTasks = u.Tasks.Count(t => t.State != TaskState.Finished),
                    LongestTask = u.Tasks.OrderByDescending(t => t.FinishedAt - t.CreatedAt).FirstOrDefault()
                })
                .FirstOrDefault();
        }

        public async Task<ImmutableArray<ProjectInfo>> GetProjectsInfo()
        {
            var composedData = await _dataComposeService.GetComposedDataAsync();

            return composedData
                .Where(p => p.Description.Length > 20 || p.Tasks.Length < 3)
                .Select(p => new ProjectInfo()
                {
                    Project = p,
                    LongestByDescTask = p.Tasks
                        .OrderByDescending(t => t.Description)
                        .FirstOrDefault(),
                    ShortestByNameTask = p.Tasks
                        .OrderBy(t => t.Name)
                        .FirstOrDefault(),
                    TotalProjectParticians = p.Team.Users?.Length ?? 0
                })
                .ToImmutableArray();
                
        }
    }
}