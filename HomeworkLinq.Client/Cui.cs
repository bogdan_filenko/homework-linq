using System;

namespace homework_linq.Client
{
    public sealed class Cui
    {
        public void ShowMainDialog()
        {
            Console.Clear();
            Console.WriteLine(
                "1. Tasks` number in the user`s project\n" +
                "2. Tasks` list for a determined user with limited name (< 45)\n" +
                "3. Finished tasks` list in the current year for a defined user\n" +
                "4. Teams` list where users older than 10\n" +
                "5. Users` sorted list with sorted tasks\n" +
                "6. User`s info\n" +
                "7. Project info\n" +
                "8. Exit\n"
            );
        }

        public void ShowUserIdDialog()
        {
            Console.Clear();
            Console.Write("Please, type user`s id:  ");
        }

        public void ShowMessage(string message)
        {
            Console.WriteLine(message);
        }
    }
}