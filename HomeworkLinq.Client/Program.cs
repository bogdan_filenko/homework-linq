﻿using System;
using AutoMapper;
using homework_linq.BLL.MappingProfiles;
using homework_linq.BLL.Models;

#nullable enable

namespace homework_linq.Client
{
    class Program
    {
        private static readonly Cui _cui = new Cui();
        private static readonly ResponseDataViewer _dataViewer = new ResponseDataViewer();

        private static bool _isExecuting = true;
        private static ComposedDataController? _dataController;

        static IMapper ConfigureMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
            });

            return config.CreateMapper();
        } 
        static async System.Threading.Tasks.Task Main()
        {
            _dataController = new ComposedDataController(ConfigureMapper());

            while (_isExecuting)
            {
                _cui.ShowMainDialog();
                char option = Console.ReadKey(true).KeyChar;

                switch(option)
                {
                    case '1':
                    {
                        int? userId = ExecuteUserIdDialog();
                        if (userId != default)
                        {
                            _dataViewer.ViewProjectsTaskNumber(await _dataController.GetProjectsTaskNumber(userId.Value));
                        }
                        _cui.ShowMessage("Press any key to continue...");
                        break;
                    }
                    case '2':
                    {
                        int? userId = ExecuteUserIdDialog();
                        if (userId != default)
                        {
                            _dataViewer.ViewCollectionData<homework_linq.BLL.Models.Task>(await _dataController.GetAllUserTasks(userId.Value));
                        }
                        _cui.ShowMessage("Press any key to continue...");
                        break;
                    }
                    case '3':
                    {
                        int? userId = ExecuteUserIdDialog();
                        if (userId != default)
                        {
                            _dataViewer.ViewCollectionData<ShortTask>(await _dataController.GetFinishedTasks(userId.Value));
                        }
                        _cui.ShowMessage("Press any key to continue...");
                        break;
                    }
                    case '4':
                    {
                        _dataViewer.ViewCollectionData<ShortTeam>(await _dataController.GetTeamsWhereUsersOlderThan10());
                        _cui.ShowMessage("Press any key to continue...");
                        break;
                    }
                    case '5':
                    {
                        _dataViewer.ViewCollectionData<User>(await _dataController.GetUsersWithSortedTasks());
                        _cui.ShowMessage("Press any key to continue...");
                        break;
                    }
                    case '6':
                    {
                        int? userId = ExecuteUserIdDialog();
                        if (userId != default)
                        {
                            _dataViewer.ViewSingleEntity<UserInfo>(await _dataController.GetUserInfo(userId.Value));
                        }
                        _cui.ShowMessage("Press any key to continue...");
                        break;
                    }
                    case '7':
                    {
                        _dataViewer.ViewCollectionData<ProjectInfo>(await _dataController.GetProjectsInfo());
                        _cui.ShowMessage("Press any key to continue...");
                        break;
                    }
                    case '8':
                    {
                        _cui.ShowMessage("Program is closing. Press any key to continue...");
                        _isExecuting = false;
                        break;
                    }
                    default:
                    {
                        _cui.ShowMessage("Unknown option. Press any key to continue...");
                        break;
                    }
                }
                Console.ReadKey(true);
            }
        }

        static int? ExecuteUserIdDialog()
        {
            _cui.ShowUserIdDialog();
            if (!int.TryParse(Console.ReadLine(), out int userId))
            {
                _cui.ShowMessage("Invalid user`s id format");
                return default;
            }
            return userId;
        }
    }
}
