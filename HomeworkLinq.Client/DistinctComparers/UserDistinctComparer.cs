using System.Collections.Generic;
using homework_linq.BLL.Models;
using System;

namespace homework_linq.Client.DistinctComparers
{
    public sealed class UserDistinctComparer : IEqualityComparer<User>
    {
        public bool Equals(User x, User y)
    {
        if (Object.ReferenceEquals(x, y))
        {
            return true;
        }

        if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null))
        {
            return false;
        }

        return x.Id == y.Id;
    }


    public int GetHashCode(User user)
    {
        if (Object.ReferenceEquals(user, null)) return 0;

        int hashUserId = user.Id.GetHashCode();

        return hashUserId;
    }
    }
}